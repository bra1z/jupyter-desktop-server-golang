# Jupyter Desktop Server with golang

https://mybinder.org/v2/git/https%3A%2F%2Fgit.disroot.org%2Fbra1z%2Fjupyter-desktop-server-golang.git/master?urlpath=desktop

Run XFCE (or other desktop environments) on a JupyterHub.

This is based on https://github.com/ryanlovett/nbnovnc and a fork of https://github.com/manics/jupyter-omeroanalysis-desktop
